from django.shortcuts import render
from django.contrib import messages
from django.http import HttpResponseRedirect
# Create your views here.

def home(request):
    messages.add_message(request, messages.INFO, 'Bienvenue.')
    return render(request, 'base.html')