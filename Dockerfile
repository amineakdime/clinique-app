# Utilisation de l'image Python officielle comme image de base
FROM python:3.12.2-slim

# Définition du répertoire de travail dans le conteneur
WORKDIR /app

# Copie des fichiers requis (par exemple requirements.txt) dans le conteneur
COPY requirements.txt .

# Installation des dépendances Python
RUN pip install --no-cache-dir -r requirements.txt

# Copie de tout le code source de l'application dans le conteneur
COPY . .

# Exposition du port 8000 pour accéder à l'application
EXPOSE 8000

# Commande pour démarrer l'application
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]